// Generated on 2013-08-10 using generator-webapp 0.2.7
'use strict';

var mountFolder = function(connect, dir) {
	return connect.static(require('path').resolve(dir));
};

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function(grunt) {
	// load all grunt tasks
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	grunt.initConfig({
		folders: {
			src: 'src',
			dist: 'dist'
		},
		watch: {
			styles: {
				files: ['<%= folders.src %>/css/{,*/}*.css'],
				tasks: ['copy:styles', 'autoprefixer']
			}
		},
		clean: {
			dist: {
				files: [{
					dot: true,
					src: [
						'.tmp',
						'<%= folders.dist %>/*',
						'!<%= folders.dist %>/.git*'
					]
				}]
			},
			server: '.tmp'
		},
		autoprefixer: {
			options: {
				browsers: ['last 1 version']
			},
			dist: {
				files: [{
					expand: true,
					cwd: '.tmp/css/',
					src: '{,*/}*.css',
					dest: '.tmp/css/'
				}]
			}
		},
		// not used since Uglify task does concat,
		// but still available if needed
		/*concat: {
			dist: {}
		},*/
		// not enabled since usemin task does concat and uglify
		// check index.html to edit your build targets
		// enable this task if you prefer defining your build targets here
		/*uglify: {
			dist: {}
		},*/
		rev: {
			dist: {
				files: {
					src: [
						'<%= folders.dist %>/js/{,*/}*.js',
						'<%= folders.dist %>/css/{,*/}*.css',
						'<%= folders.dist %>/img/{,*/}*.{png,jpg,jpeg,gif,webp}',
						'<%= folders.dist %>/css/fonts/*'
					]
				}
			}
		},
		useminPrepare: {
			options: {
				dest: '<%= folders.dist %>'
			},
			html: '<%= folders.src %>/index.html'
		},
		usemin: {
			options: {
				dirs: ['<%= folders.dist %>']
			},
			html: ['<%= folders.dist %>/{,*/}*.html'],
			css: ['<%= folders.dist %>/css/{,*/}*.css']
		},
		imagemin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= folders.src %>/img',
					src: '{,*/}*.{png,jpg,jpeg}',
					dest: '<%= folders.dist %>/img'
				}]
			}
		},
		svgmin: {
			dist: {
				files: [{
					expand: true,
					cwd: '<%= folders.src %>/img',
					src: '{,*/}*.svg',
					dest: '<%= folders.dist %>/img'
				}]
			}
		},
		cssmin: {
			// This task is pre-configured if you do not wish to use Usemin
			// blocks for your CSS. By default, the Usemin block from your
			// `index.html` will take care of minification, e.g.
			//
			//     <!-- build:css({.tmp,app}) styles/main.css -->
			//
			// dist: {
			//     files: {
			//         '<%= folders.dist %>/css/main.css': [
			//             '.tmp/css/{,*/}*.css',
			//             '<%= folders.src %>/css/{,*/}*.css'
			//         ]
			//     }
			// }
		},
		htmlmin: {
			dist: {
				options: {
					/*removeCommentsFromCDATA: true,
					// https://github.com/yeoman/grunt-usemin/issues/44
					//collapseWhitespace: true,
					collapseBooleanAttributes: true,
					removeAttributeQuotes: true,
					removeRedundantAttributes: true,
					useShortDoctype: true,
					removeEmptyAttributes: true,
					removeOptionalTags: true*/
				},
				files: [{
					expand: true,
					cwd: '<%= folders.src %>',
					src: '*.html',
					dest: '<%= folders.dist %>'
				}]
			}
		},
		// Put files not handled in other tasks here
		copy: {
			dist: {
				files: [{
					expand: true,
					dot: true,
					cwd: '<%= folders.src %>',
					dest: '<%= folders.dist %>',
					src: [
						'*.{ico,png,txt}',
						'.htaccess',
						'bower_components/**/*',
						'images/{,*/}*.{webp,gif}',
						'styles/fonts/*'
					]
				}]
			},
			styles: {
				expand: true,
				dot: true,
				cwd: '<%= folders.src %>/styles',
				dest: '.tmp/css/',
				src: '{,*/}*.css'
			}
		},
		concurrent: {
			server: [
				'copy:styles'
			],
			test: [
				'copy:styles'
			],
			dist: [
				'copy:styles',
				'imagemin',
				'svgmin',
				'htmlmin'
			]
		}
	});

	grunt.registerTask('build', [
		'clean:dist',
		'useminPrepare',
		'concurrent:dist',
		'autoprefixer',
		'concat',
		'cssmin',
		'uglify',
		'copy:dist',
		'rev',
		'usemin'
	]);

	grunt.registerTask('default', [
		'build'
	]);
};
